package com.maximov.carshop;

import com.maximov.models.Car;
import com.maximov.models.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StoreTest {
    private static Store store;

    @BeforeEach
    public void initStore(){
        store = new Store();
    }

    @org.junit.jupiter.api.Test
    void createCar() {
        store.createCar(100, "Lada", "ABC");

        List<Car> result = store.getFreeCars();
        assertEquals(1, result.size());
        result.stream().forEach((car) -> {
            assertEquals(100, car.getPrice());
            assertEquals("Lada", car.getModel());
            assertEquals("ABC", car.getRegNum());
        });
    }

    @org.junit.jupiter.api.Test
    void sellCar() throws CarNotFoundException {
        assertThrows(CarNotFoundException.class,
                () ->
                    store.sellCar("GAZ", "Tolian", "Toliani", ""));

        store.createCar(100, "Lada", "ABC");

        store.sellCar("Lada", "Tolian", "Toliani", "");

        assertTrue(store.getFreeCars().size() == 0);

        store.getOrders().stream().filter((order) -> order.getCar().getModel() == "Lada"
        && order.getCar().getPrice() == 100
        && order.getCar().getRegNum().equals("ABC"));

        assertTrue(store.getContractList().size() == 1);

        assertTrue(store.getContractList().values().stream().anyMatch(
                (client) -> client.getFirsName().equals("Tolian")
                            && client.getLastName().equals("Toliani")
                            && client.getPhoneNumber().equals("")));
    }

    @Test
    void getContractList(){
        assertNotNull(store.getContractList());
        assertTrue(store.getContractList().size() == 0);
    }

    @org.junit.jupiter.api.Test
    void getOrders() {

    }

    @org.junit.jupiter.api.Test
    void getFreeCars() {

    }

    @org.junit.jupiter.api.Test
    void getClients() {

    }

    @org.junit.jupiter.api.Test
    void save() {

    }

    @org.junit.jupiter.api.Test
    void recover() {

    }

    @Test
    void getFirstOrder(){

    }
}