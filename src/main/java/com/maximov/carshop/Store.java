package com.maximov.carshop;

import com.maximov.datamanager.DataManager;
import com.maximov.models.*;
import org.apache.log4j.*;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.*;

public class Store {
	private HashMap<Order, Client> contractList = new HashMap<>();
	private HashSet<Car> cars = new HashSet<>(32);
	private HashSet<Client> clients= new HashSet<>(256);

	private static Logger logger = Logger.getLogger(Store.class);

	private static final String FILE_CONTRACTS = "fileContracts.txt";
	private static final String FILE_CARS = "fileCars.txt";
	private static final String FILE_CLIENTS = "fileClients.txt";

	public void createCar(int price, String model, String regNumber){
		Car car = new Car(price, model, regNumber);
		cars.add(car);
	}

	public void sellCar(String model, String firsName,
						String lastName, String phoneNumber) throws CarNotFoundException {
		Client orderingClient = new Client(firsName, lastName, phoneNumber);
		clients.add(orderingClient);

		Car orderedCar = null;
		for(Car car:cars){
			if(car.getModel().equals(model)){
				orderedCar = car;
				break;
			}
		}

		if(orderedCar != null){
			Random random = new Random();
			Order order = new Order(orderedCar, orderedCar.getPrice() * 2,
					random.nextLong(), (short) 80);
			contractList.put(order, orderingClient);
			cars.remove(orderedCar);
		}
		else{
			CarNotFoundException ex = new CarNotFoundException();
			logger.error(String.format("Car with model \"%1s\" did not find", model), ex);
			throw ex;
		}
	}

	public Set<Order> getOrders(){
		return contractList.keySet();
	}

	public List<Car> getFreeCars(){
		ArrayList<Car> list = new ArrayList<>();
		for (Car car : this.cars){
			list.add(car);
		}

		return list;
	}

	public void getClients(){
		for (Client client : this.clients){
			System.out.println(client.getLastName());
		}
	}

	public void save(){
		DataManager.serialize(cars, FILE_CARS);
		DataManager.serialize(clients, FILE_CLIENTS);
		DataManager.serialize(contractList, FILE_CONTRACTS);
	}

	public void recover(){
		DataManager.deserialize(FILE_CLIENTS, clients);
		DataManager.deserialize(FILE_CARS, cars);
		DataManager.deserialize(FILE_CONTRACTS, contractList);
	}

	public Map<Order, Client> getContractList(){
		return this.contractList;
	}
}
