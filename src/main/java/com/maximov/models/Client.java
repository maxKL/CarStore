package com.maximov.models;

import java.io.Serializable;

public class Client implements Serializable {
    private String firsName;
    private String lastName;
    private String phoneNumber;

    public Client(String firsName, String lastName, String phoneNumber) {
        this.firsName = firsName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }

        if(!(obj instanceof Client)){
            return false;
        }

        Client client = (Client) obj;
        if(this.getFirsName().equals(client.getFirsName())
                && this.getLastName().equals(client.getLastName())){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getFirsName().hashCode() * 21 + this.getLastName().hashCode();
    }
}
