package com.maximov.datamanager;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.*;

public class DataManager {
    private static final Logger logger = Logger.getLogger(DataManager.class);

    public static void serialize(Collection<? extends Serializable> list, String fileName){
        fileName = "C/: not";
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos) ){
            for (Serializable element:
                 list) {
                oos.writeObject(element);
            }
        } catch (FileNotFoundException e) {
            logger.error(String.format("Файл \"%1s\" не найден", fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <K extends Serializable, V extends Serializable> void serialize(Map<K, V> map, String fileName){
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos) ){
            oos.writeObject(map);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T extends Serializable> void deserialize(String fileName, Collection<T> outCollection){
        try (FileInputStream fos = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fos) ){

            Serializable deserializedObject;
            while ((deserializedObject = (Serializable) ois.readObject()) != null) {
                outCollection.add((T) deserializedObject);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (EOFException e){
            return;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static <K extends Serializable, V extends Serializable> void deserialize(String fileName, Map<K, V> outMap){
        try (FileInputStream fos = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fos) ){

            for(Map.Entry<K, V> entry : ((Map<K, V>)ois.readObject()).entrySet()){
                outMap.put(entry.getKey(), entry.getValue());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
