package com.maximov.utils.log4j;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;

public class MailLayout extends PatternLayout {

    @Override
    public String format(LoggingEvent event){
        String message = super.format(event);
        String stackTraceString = getStackTraceString(event);
        return message + stackTraceString;
    }

    private String getStackTraceString(LoggingEvent event) {
        String stackTraceString = "";
        ThrowableInformation throwableInformation = event.getThrowableInformation();
        if(throwableInformation != null){
            Throwable throwable = throwableInformation.getThrowable();
            for(StackTraceElement stElement : throwable.getStackTrace()){
                stackTraceString += stElement.toString() + "\r\n";
            }
        }
        return stackTraceString;
    }
}
