package com.maximov.utils.log4j;

import com.maximov.utils.mailServer.MailServer;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

import javax.mail.MessagingException;
import java.io.IOException;

public class MailAppender extends AppenderSkeleton {
    private MailServer mailServer;

    public MailAppender() {
        try {
            mailServer = new MailServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void append(LoggingEvent loggingEvent) {
        String message = layout.format(loggingEvent);
        try {
            mailServer.sendMessage("CarStore " + loggingEvent.getLevel().toString(), message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return false;
    }
}
