package com.maximov.utils.mailServer;

import java.io.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class MailServer {

    private Session session;
    private String username;

    public MailServer() throws IOException {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        try(InputStream stream = new FileInputStream("mailConfig.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, "utf-8"))) {
            username = bufferedReader.readLine();
            String password = bufferedReader.readLine();
            session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
        }
    }

    public void sendMessage(String subject, String messageText) throws MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(username));
        message.setSubject(subject);
        message.setText(messageText);

        Transport.send(message);
    }
}
