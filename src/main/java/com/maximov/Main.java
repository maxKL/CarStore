package com.maximov;

import com.maximov.carshop.CarNotFoundException;
import com.maximov.carshop.Store;
import org.apache.log4j.xml.DOMConfigurator;


public class Main {

    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }

    public static void main(String[] args) throws CarNotFoundException {
        Store store = new Store();

        store.createCar(500000, "Kia Rio", "B146AA");
        store.createCar(550000, "Rehno Logan", "x777xx");
        store.sellCar("Kia Rio", "John", "Konner",
                "89999999999");

        store.save();

        System.out.println("Original store:");
        showStore(store);

        Store recoveredStore = new Store();
        recoveredStore.recover();

        System.out.println("Recovered store:");
        showStore(recoveredStore);

        store.sellCar("Batmobile", "Bruce", "Waine",
                "89999999966");


    }

    private static void showStore(Store store){
        store.getOrders();
        store.getFreeCars();
        store.getClients();
    }
}
